var readBooksPromise = require('./promise.js')

var books = [
    {name: 'LOTR', timeSpent: 3000},
    {name: 'Fidas', timeSpent: 2000},
    {name: 'Kalkulus', timeSpent: 4000}
]

var indeks = 0
function read(time){
  if(indeks < books.length){
    readBooksPromise(time, books[indeks])
    .then(result => read (result))
    .catch(error => console.log(error))
  }
  indeks++
}
read(10000);
