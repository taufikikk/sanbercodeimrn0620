// di index.js
var readBooks = require('./callback.js')

var books = [
    {name: 'LOTR', timeSpent: 3000},
    {name: 'Fidas', timeSpent: 2000},
    {name: 'Kalkulus', timeSpent: 4000}
]

var indeks = 0
function read(time){
  if(indeks < books.length){
    readBooks(time, books[indeks], sisaWaktu => read(sisaWaktu))
  }
  indeks++
}
read(10000)
