// Soal 1
function arrayToObject(arr) {
  var now = new Date()
  var thisYear = now.getFullYear() // 2020 (tahun sekarang)
  var age = "";
  for (var i = 0; i < arr.length; i++) {
    for (var j = 0; j < arr[i].length; j++) {
      if (arr[i][3] > thisYear || !arr[i][3]) {
        age = "Invalid birth year"
      }
      else {
        age = thisYear - arr[i][3]
      }
    }
  var data = {}
  data.firstName = arr[i][0]
  data.lastName = arr[i][1]
  data.gender = arr[i][2]
  data.age = age
  console.log((i + 1) + " ." + arr[i][0] + " " + arr[i][1], data)
  }
}

// Driver Code
var people = [ ["Bruce", "Banner", "male", 1975], ["Natasha", "Romanoff", "female"] ]
arrayToObject(people)
/*
    1. Bruce Banner: {
        firstName: "Bruce",
        lastName: "Banner",
        gender: "male",
        age: 45
    }
    2. Natasha Romanoff: {
        firstName: "Natasha",
        lastName: "Romanoff",
        gender: "female".
        age: "Invalid Birth Year"
    }
*/

var people2 = [ ["Tony", "Stark", "male", 1980], ["Pepper", "Pots", "female", 2023] ]
arrayToObject(people2)
/*
    1. Tony Stark: {
        firstName: "Tony",
        lastName: "Stark",
        gender: "male",
        age: 40
    }
    2. Pepper Pots: {
        firstName: "Pepper",
        lastName: "Pots",
        gender: "female".
        age: "Invalid Birth Year"
    }
*/

// Error case
arrayToObject([]) // ""


// Soal 2
function shoppingTime(memberId, money) {
  var produk = [["Sepatu Stacattu", 1500000], ["Sweater Uniklooh", 175000], ["Baju Zoro", 500000],
  ["Baju H&N", 250000], ["Casing Handphone", 50000]];
  var changeMoney = 0;
  var listPurchased = [];
  var keranjang = {};
  produk = produk.sort(function (a, b) { return b[1] - a[1] });

  if (memberId != '' && money != null) {
  if (money >= 50000) {
    changeMoney = money;
    for (let k = 0; k < produk.length; k++) {
      if (changeMoney >= produk[k][1]) {
        changeMoney -= produk[k][1]
        listPurchased.push(produk[k][0])
      }
    }

    keranjang.memberId = memberId;
    keranjang.money = money;
    keranjang.listPurchased = listPurchased;
    keranjang.changeMoney = changeMoney;

    return keranjang
    }
    else {
      return "Mohon maaf, uang tidak cukup";
    }
  }
  else {
    return "Mohon maaf, toko X hanya berlaku untuk member saja";
  }
}

// TEST CASES
console.log(shoppingTime('1820RzKrnWn08', 2475000));
  //{ memberId: '1820RzKrnWn08',
  // money: 2475000,
  // listPurchased:
  //  [ 'Sepatu Stacattu',
  //    'Baju Zoro',
  //    'Baju H&N',
  //    'Sweater Uniklooh',
  //    'Casing Handphone' ],
  // changeMoney: 0 }
console.log(shoppingTime('82Ku8Ma742', 170000));
//{ memberId: '82Ku8Ma742',
// money: 170000,
// listPurchased:
//  [ 'Casing Handphone' ],
// changeMoney: 120000 }
console.log(shoppingTime('', 2475000)); //Mohon maaf, toko X hanya berlaku untuk member saja
console.log(shoppingTime('234JdhweRxa53', 15000)); //Mohon maaf, uang tidak cukup
console.log(shoppingTime()); ////Mohon maaf, toko X hanya berlaku untuk member saja


// Soal 3
function naikAngkot(arrPenumpang) {
  rute = ['A', 'B', 'C', 'D', 'E', 'F'];
  var output = [];
  var naik = 0;
  var tujuan = 0;
  var indexNaik = 0;
  var indexTujuan = 0;
  var totalPrice = 0;
  rute = rute.sort(function (a, b) { return a < b ? -1 : 1 })

  if (arrPenumpang !== null || arrPenumpang.length != 0) {
    for (var i = 0; i < arrPenumpang.length; i++) {
      naik = arrPenumpang[i][1];
      tujuan = arrPenumpang[i][2];

      for (var j = 0; j < rute.length; j++) {
        if (naik == rute[j]) {
                hargaNaik = j + 1;
        }
        if (tujuan == rute[j]) {
          hargaTujuan = j + 1;
        }
      }
  var list = {};
  totalPrice = (hargaTujuan - hargaNaik) * 2000;
  list.penumpang = arrPenumpang[i][0];
  list.naikDari = naik;
  list.tujuan = tujuan;
  list.bayar = totalPrice;
  output.push(list);
    }
  }
  return output
}


//TEST CASE
console.log(naikAngkot([['Dimitri', 'B', 'F'], ['Icha', 'A', 'B']]));
// [ { penumpang: 'Dimitri', naikDari: 'B', tujuan: 'F', bayar: 8000 },
//   { penumpang: 'Icha', naikDari: 'A', tujuan: 'B', bayar: 2000 } ]

console.log(naikAngkot([])); //[]
