/**
 * Berikut soal quiz kali ini, terdiri dari 3 Soal
 * Kerjakan dengan sebaik mungkin, dengan menggunakan metode yang telah dipelajari,
 * Tidak diperkenankan untuk menjawab hanya dengan console.log('teks jawaban');
 * maupun dengan terlebih dahulu memasukkannya ke dalam variabel, misal var a = 'teks jawaban'; console.log(a);
 *
 * Terdapat tambahan poin pada setiap soal yang dikerjakan menggunakan sintaks ES6 (+5 poin)
 * Jika total nilai Anda melebihi 100 (nilai pilihan ganda + coding), tetap akan memiliki nilai akhir yaitu 100
 *
 * Selamat mengerjakan
*/

/*==========================================
  1. SOAL CLASS SCORE (10 poin + 5 Poin ES6)
  ==========================================
  Buatlah sebuah class dengan nama Score. class Score tersebut memiliki properti "subject", "points", dan "email".
  "points" dapat di input berupa number (1 nilai) atau array of number (banyak nilai).
  tambahkan method average untuk menghitung rata-rata dari parameter points ketika yang di input berupa array (lebih dari 1 nilai)
*/

class Score {
  constructor(points){
    this.subject;
    this.points = points;
    this.email;
  }

  average(){
    var point = this.points;
    if(Array.isArray(point)){
      var i = point.length;
      var j = 0;
      for (let k = 0; k < i; k++){
      j += point[k]
      }
    return Number(j / i).toFixed(1)
    }
    return point;
  }
}

/*===========================================
  2. SOAL Create Score (10 Poin + 5 Poin ES6)
  ===========================================
  Membuat function viewScores yang menerima parameter data berupa array multidimensi dan subject berupa string
  Function viewScores mengolah data email dan nilai skor pada parameter array
  lalu mengembalikan data array yang berisi object yang dibuat dari class Score.
  Contoh:

  Input

  data :
  [
    ["email", "quiz-1", "quiz-2", "quiz-3"],
    ["abduh@mail.com", 78, 89, 90],
    ["khairun@mail.com", 95, 85, 88]
  ]
  subject: "quiz-1"

  Output
  [
    {email: "abduh@mail.com", subject: "quiz-1", points: 78},
    {email: "khairun@mail.com", subject: "quiz-1", points: 95},
  ]
*/

const data = [
  ["email", "quiz-1", "quiz-2", "quiz-3"],
  ["abduh@mail.com", 78, 89, 90],
  ["khairun@mail.com", 95, 85, 88],
  ["bondra@mail.com", 70, 75, 78],
  ["regi@mail.com", 91, 89, 93]
]

let viewScores = (data, subject) => {
  var p_email = 0;
  var p_subject = 0;
  var arr = []
  switch (subject) {
    case "quiz-1":
    p_subject=1;
    break;
    case "quiz-2":
    p_subject=2;
    break;
    case "quiz-3":
    p_subject=3;
    break;
    default:

  }
  for(let i = 1; i < data.length; i++){
    var dataScore = new Score(data[i][p_subject]);
    var obj = {};
    obj.email = data[i][p_email];
    obj.subject = subject;
    obj.points = dataScore.average();
    arr.push(obj)
  }
  console.log(arr);
}

// TEST CASE
viewScores(data, "quiz-1")
viewScores(data, "quiz-2")
viewScores(data, "quiz-3")

/*==========================================
  3. SOAL Recap Score (15 Poin + 5 Poin ES6)
  ==========================================
    Buatlah fungsi recapScore yang menampilkan perolehan nilai semua student.
    Data yang ditampilkan adalah email user, nilai rata-rata, dan predikat kelulusan.
    predikat kelulusan ditentukan dari aturan berikut:
    nilai > 70 "participant"
    nilai > 80 "graduate"
    nilai > 90 "honour"

    output:
    1. Email: abduh@mail.com
    Rata-rata: 85.7
    Predikat: graduate

    2. Email: khairun@mail.com
    Rata-rata: 89.3
    Predikat: graduate

    3. Email: bondra@mail.com
    Rata-rata: 74.3
    Predikat: participant

    4. Email: regi@mail.com
    Rata-rata: 91
    Predikat: honour

*/

var recapScores = data => {
  var arr = [];
  for(let i = 1; i < data.length; i++){
    var array2 = [];
    var object2 = {};
    var avg = 0;


    for (let j = 1; j < data[i].length; j++){
      array2[j - 1] = data[i][j];
  }
  let datascore = new Score(array2);
  avg = datascore.average();
  if(avg > 0 && avg < 80){
    object2.predikat = "participant";
  }
  else if (avg > 80 && avg < 90){
    object2.predikat = "graduate";
  }
  else {
    object2.predikat = "honour";
  }
  object2.indeks = i;
  object2.email = data[i][0];
  object2.ratarata = avg;
  arr.push(object2)
}

console.log("output: ")
for (let i = 0; i < arr.length; i++){
  console.log(`${arr[i].indeks}. Email: ${arr[i].email}
Rata-rata: ${arr[i].ratarata}
Predikat: ${arr[i].predikat}`)
  console.log()
}
}

recapScores(data);
