//soal 1
var nama = "Taufiq";
var peran = "Werewolf";
if ( nama != null ) {
    console.log("Halo " + nama +  ", Pilih peranmu untuk memulai game!");
    if( peran == "Penyihir") {
        console.log("Selamat datang di Dunia Werewolf, " + nama);
        console.log("Halo Penyihir " + nama +
        ", kamu dapat melihat siapa yang menjadi werewolf!");
    } else if ( peran == "Guard" ) {
      console.log("Selamat datang di Dunia Werewolf, " + nama);
      console.log("Halo Guard "+ nama +
      ", kamu akan membantu melindungi temanmu dari serangan werewolf.");
    } else if ( peran == "Werewolf" ) {
      console.log("Selamat datang di Dunia Werewolf, Junaedi");
      console.log("Halo Werewolf " + nama +
      ", Kamu akan memakan mangsa setiap malam!");
    }
} else {
    console.log("Nama harus diisi!")
}

//soal 2
var tanggal = 3;
var bulan = 1;
var tahun = 1900;

switch(true){
  case ( tanggal > 0 && tanggal < 32):  { console.log(tanggal);
    break; }
  default: { console.log('tanggal yang anda masukkan salah'); }
}

switch(bulan) {
  case 1:   { console.log('Januari'); break; }
  case 2:   { console.log('Februari'); break; }
  case 3:   { console.log('Maret'); break; }
  case 4:   { console.log('April'); break; }
  case 5:   { console.log('Mei'); break; }
  case 6:   { console.log('Juni'); break; }
  case 7:   { console.log('Juli'); break; }
  case 8:   { console.log('Agustus'); break; }
  case 9:   { console.log('September'); break; }
  case 10:   { console.log('Oktober'); break; }
  case 11:   { console.log('November'); break; }
  case 12:   { console.log('Desember'); break; }
  default:  { console.log('Bulan yang anda masukkan salah!'); }}

  switch(true){
    case ( tahun >= 1900 && tahun <= 2200):  { console.log(tahun);
      break; }
    default: { console.log('tahun yang anda masukkan salah'); }
  }
