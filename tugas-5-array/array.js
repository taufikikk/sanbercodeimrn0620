// Soal 1
function range(startNum, finishNum, step = startNum <= finishNum ? 1 : -1) {
  var result = [];
  if (!finishNum) {
    return -1;
  }
  else {
    for (var i = startNum; step >= 0 ? i <= finishNum : i >= finishNum; i+=step) {
      result.push(i);
    }
    return result;
  }
}

console.log(range(1, 10)) //[1, 2, 3, 4, 5, 6, 7, 8, 9, 10]
console.log(range(1)) // -1
console.log(range(11,18)) // [11, 12, 13, 14, 15, 16, 17, 18]
console.log(range(54, 50)) // [54, 53, 52, 51, 50]
console.log(range()) // -1
console.log()


// Soal 2
function rangeWithStep(startNum, finishNum, step, k = startNum <= finishNum ? step : -step) {
  var result = [];
  for (var i = startNum; k >= 0 ? i <= finishNum : i >= finishNum; i+=k) {
    result.push(i);
  }
  return result;
}
console.log(rangeWithStep(1, 10, 2)) // [1, 3, 5, 7, 9]
console.log(rangeWithStep(11, 23, 3)) // [11, 14, 17, 20, 23]
console.log(rangeWithStep(5, 2, 1)) // [5, 4, 3, 2]
console.log(rangeWithStep(29, 2, 4)) // [29, 25, 21, 17, 13, 9, 5]
console.log()


// Soal 3
function sum(startNum, finishNum, step, j = startNum <= finishNum ? 1 : -1, k = startNum <= finishNum ? step : -step) {
  var result = [];
  if (!startNum && !finishNum && !step){
    return 0;
  }
  else if(!finishNum && !step){
    return startNum;
  }
  else if (!step) {
    for (var i = startNum; j >= 0 ? i <= finishNum : i >= finishNum; i+=j) {
      result.push(i);
    }
  }
  else {
    for (var i = startNum; k >= 0 ? i <= finishNum : i >= finishNum; i+=k) {
      result.push(i);
    }
  }
  var sumvar = result.reduce((total, value) => total + value, 0);
  return sumvar;
}


console.log(sum(1,10)) // 55
console.log(sum(5, 50, 2)) // 621
console.log(sum(15,10)) // 75
console.log(sum(20, 10, 2)) // 90
console.log(sum(1)) // 1
console.log(sum()) // 0
console.log()


// Soal 4
function dataHandling(input){
  var data='';
  for (var i=0; i<input.length; i++ ){
    data += 'Nomor ID: ' + input[i][0] + '\n' +
                'Nama Lengkap: ' + input[i][1] + '\n' +
                'TTL: ' + input[i].slice(2,4).join(' ') + '\n' +
                'Hobi: ' + input[i][4]+ '\n \n';
  }
  return data;
}

var input = [
                ["0001", "Roman Alamsyah", "Bandar Lampung", "21/05/1989", "Membaca"],
                ["0002", "Dika Sembiring", "Medan", "10/10/1992", "Bermain Gitar"],
                ["0003", "Winona", "Ambon", "25/12/1965", "Memasak"],
                ["0004", "Bintang Senjaya", "Martapura", "6/4/1970", "Berkebun"]
            ]

console.log(dataHandling(input));


// Soal 5
function balikKata(str) {
  var normal = str
  var kata = ""
  for (var i = str.length -1; i >= 0; i--){
    kata = kata + normal[i]
  }
  return kata;
}

console.log(balikKata("Kasur Rusak")) // kasuR rusaK
console.log(balikKata("SanberCode")) // edoCrebnaS
console.log(balikKata("Haji Ijah")) // hajI ijaH
console.log(balikKata("racecar")) // racecar
console.log(balikKata("I am Sanbers")) // srebnaS ma I
console.log()


// Soal 6
var input = ["0001", "Roman Alamsyah", "Bandar Lampung", "21/05/1989", "Membaca"];
   function dataHandling2(input) {
     input.splice(1, 1, "Roman Alamsyah Elsharawy");
     input.splice(2, 1, "Provinsi Bandar Lampung")
     input.splice(4, 1, "Pria");
     input.splice(5, 0, "SMA Internasional Metro");
     console.log(input);
     var angkaBulan = input[3].split('/');
     var namaBulan;
     switch (angkaBulan[1]) {
       case '01':
         namaBulan = 'Januari';
         break;
       case '02':
         namaBulan = 'Februari';
         break;
       case '03':
         namaBulan = 'Maret';
         break;
       case '04':
         namaBulan = 'April';
         break;
       case '05':
         namaBulan = 'Mei';
         break;
       case '06':
         namaBulan = 'Juni';
         break;
       case '07':
         namaBulan = 'Juli';
         break;
       case '08':
         namaBulan = 'Agustus';
         break;
       case '09':
         namaBulan = 'September';
         break;
       case '10':
         namaBulan = 'Oktober';
         break;
       case '11':
         namaBulan = 'November';
         break;
       case '12':
         namaBulan = 'Desember';
         break;
       default:
     }
     console.log(namaBulan);
    var tanggal = angkaBulan.join("-");
    angkaBulan.sort(function(a, b) {
       return b - a
     });
     console.log(angkaBulan);
     console.log(tanggal);
     var data = input.toString()
     var nama = data.slice(5,19)
     console.log(nama);
   }
   dataHandling2(input);
