// Soal 1
var a = 2;
var b = 0;

console.log("LOOPING PERTAMA");
while(b < 20){
  b += a;
  console.log(b + " - I love coding")
}

console.log("LOOPING KEDUA")
while(b > 2){
  b -= a;
  console.log(b + " - I will become a mobile developer")
}
console.log()


// Soal 2
console.log("OUTPUT")
for (i = 1; i < 21; i++) {
    if ((i % 3 === 0) && (i % 2 !== 0)) {
        console.log(i + ' - I Love Coding')
    }
    else if (i % 2 !== 0) {
        console.log(i + ' - Santai')
    }
    else if (i % 2 === 0) {
        console.log(i + ' - Berkualitas')
    }
}
console.log()


// Soal 3
var panjang = 4;
var lebar = 8;
for (var i = 0; i < panjang; i++) {
    for ( var j = 0; j < lebar; j++) {
        process.stdout.write("#");
    }
    console.log("");
}
console.log()


// Soal 4
var j = 0
for (i = 1; i < 8; i++) {
    for (j = 0; j < i; j++) {
        process.stdout.write("#")
    }
    console.log()
}
console.log()


// Soal 5
var size = 8;

var board = "";

for (var y = 0; y < size; y++) {
  for (var x = 0; x < size; x++) {
    if ((x + y) % 2 == 0)
      board += " ";
    else
      board += "#";
  }
  board += "\n";
}

console.log(board);
